"""This module just contains one TrainingManager class.
"""

import cfg as cfg


class TrainingManager:
	''' This class contains the basic training functionalities'''

	def __init__(self):
		self.data_folder=cfg.data_folder


	def trainModel(self, model, number_of_batches=None):
		"""
		Train model for a number of batches

		Args:
			model (object):
				Model to be trained
			number_of_batches (int):
				Number of batches the model will be trained on, if 'None' the entire training set will be used (one epoch)
		Returns:
			metrics (list):  
				A list of metrics (e.g. loss, accuracy)
		"""
		return 0


	def validateModel(self, model):
		"""
		Validate model

		Args:
			model (object):
				Model to be validated
		Returns:
			metrics (list):  
				A list of metrics (e.g. loss, accuracy)
		"""
		return 0

	def predict(self, model, input_data):
		"""
		Make a single prediction

		Args:
			model (object):
				Model to be used
			input_data (list):
				Input data to be used for prediction
		Returns:
			predictions (list):  
				Prediction
		"""
		return 0

	def _batchGenerator(self):
		"""
		Generator to create training batches
		"""
		#while True:
		#
		#	yield

		return 0
