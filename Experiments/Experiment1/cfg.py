
"""This module contains configurations
"""

#######
###Path settings
######
data_folder="" 					#folder containing the training and validation data

######
###Model parameters
######
#number_of_inputs=1 
#number_of_outputs=1
#timesteps=1
#...

######
###Training parameters
######
epochs=10						#how many epochs to train
number_of_iterations=5			#how many iterations(batches) before validation 
								#if 'None an entire epoch is trained'