"""This module contains the model definition.
"""
import cfg as cfg

class Model:
	''' This class is used to create the Neural Network model to be trained.'''
	def __init__(self):
		self.NNmodel=self._createNN()

	def _createNN(self):
		"""
		Create a Neural Network (e.g. with keras)
		"""
		return 0

	def train_on_batch(self,inputs,targets):
		"""
		Train the model for one batch

		Args:
			inputs:
				List of inputs
			targets:
				List of corresponding targets
			
		Returns:
			metrics (list):  
				A list of metrics (e.g. loss, accuracy)
		"""
		return self.NNmodel.train_on_batch(inputs,targets)

	def test_on_batch(self,inputs,targets):
		"""
		Test the model for one batch

		Args:
			inputs:
				List of inputs
			targets:
				List of corresponding targets
			
		Returns:
			metrics (list):  
				A list of metrics (e.g. loss, accuracy)
		"""
		return self.NNmodel.test_on_batch(inputs,targets)

	def save(self,target_path):
		"""
		Save model

		Args:
			target_path:
				Path to saving destination file 			
		"""
		self.NNmodel.save(traget_path)

	def load(self,path):
		"""
		Load model

		Args:
			traget_path:
				Path to saved model file 			
		"""
		print("load weights")
		#self.NNmodel.load(traget_path)
