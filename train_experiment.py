
import sys
import numpy as np
import csv
import os


#Get experiment name 
assert (len(sys.argv)>=2), "Please specify experiment name!"
experiment_name=sys.argv[1]
assert (os.path.isdir("./Experiments/"+sys.argv[1])), "Experiment '"+experiment_name+"' does not exist!"


#Import modules
sys.path.insert(0, './Experiments/'+experiment_name)
from training_manager import TrainingManager
from model_definition import Model
import cfg as cfg

#Set path to save results and create folder
result_path = "Experiments/"+experiment_name+"/Result/"
if not os.path.exists(result_path):
	os.makedirs(result_path)

#Build network model
my_model=Model()
training_manager=TrainingManager()

metrics_file=result_path+"metrics_history.npy"

train_metrics_history=[]
val_metrics_history=[]

if os.path.isfile(metrics_file):
	user_input = raw_input("Existing metrics history file found. Do you want to continue the last learning run? (y,n)")
	while(user_input!='y' and user_input!='n'):
		user_input = raw_input("Please anser with (y,n)")
	if user_input=='y':
		old_metrics=np.load(metrics_file)
		old_metrics=old_metrics.item()
		train_metrics_history=old_metrics['training']
		val_metrics_history=old_metrics['validation']
		my_model.load_weights(result_path+"weights.h5")


#Training Loop:
for e in range(cfg.epochs):

	#Train
	new_train_metric=training_manager.trainModel(my_model,cfg.number_of_iterations)
	train_metrics_history.append(new_train_metric)

	#Validate
	new_val_metrics=training_manager.validateModel(my_model)
	val_metrics_history.append(new_val_metrics)

	#Save
	np.save(metrics_file,{'training':train_metrics_history, 'validation':val_metrics_history})
	if new_val_metrics[0]>=np.max(np.array(val_metrics_history)[:,0]):
		my_model.save(result_path+"weights.h5")


