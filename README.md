### Description
This project aims at providing a generic model selection framework for machine learning tasks. The focus is training of neural networks but it can generally be used for any other classifier or machine learning algorithm. The idea is to keep different experiments decoupled (the code, parameters and results) to be able to reconstruct each training process. Only the code for the general learning procedure is shared between experiments.

### Structure
Each experiment is located in an unique folder in '/Experiments/'. In this Initial commit a dummy experiment is included to show the interfaces. A training run can be started with the train_experiment.py script and the experiment name as an argument.